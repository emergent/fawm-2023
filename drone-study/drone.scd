// a drone study
"setup.scd".loadRelative;

{PinkNoise.ar(0.2!2) * Env.perc.kr(2)}.play(outbus: ~bus[\reverb]);
~revSynth.set(\drywet, 0.75);
~revSynth.set(\time, 0.9);
~revSynth.set(\revAmp, 0.dbamp);

// instruments
(
SynthDef(\saw, {
	arg dry = 0.5, rq = 0.5;
	var sig, env, dt;
	
	env = Env.adsr(\atk.ir(0.1), \dec.ir(0.2), \suslev.ir(1), \rel.ir(2)).kr(2, \gate.kr(1));

	dt = LFNoise2.ar(0.25!5).bipolar(\dtune.kr(0.15)).midiratio; 

	sig = VarSaw.ar(
		\freq.kr(220) * dt,
		0.0001,
		\width.kr(0.001),
		0.05
	).sum;

	sig = RLPF.ar(
		sig, 
		SinOsc.ar(
			\sweep.kr(1/20),
			\sweepphase.kr(0)
		).range(
			\sweepmin.kr(800),\sweepmax.kr(2400)
		),
		rq,
		1/(rq.sqrt)
	);

	sig = sig * env * \amp.kr(-6.dbamp);

	//sig = Splay.ar(sig);
	sig = Pan2.ar(sig, 0);

	Out.ar(\out.ir(0), sig * dry);
	Out.ar(\fx.ir(0), sig * (1- dry));
}).add;
)

~saw = Synth(\saw, [\out, ~out, \fx, ~bus[\reverb], \atk, 0.5, \amp, -12.dbamp], ~src);
~saw.release;

(
~base = Pbind(
	\instrument, \saw,
	\scale, Scale.dorian(\pythagorean),
	\ctranspose, -4,
	\group, ~src,
	\out, ~out,
	\fx, ~bus[\reverb]
);

~drone = Pbind(
	\octave, 3,
	\degree, Pxrand((0..7), inf).clump(2),
	\legato, Pwhite(0.95, 1.5),
	\dur, 16,
	\amp, -16.dbamp,
	\atk, 0.75,
	\rel, 2,
	\sweep, Pwhite(1/20, 8/60),
	\rq, Pwhite(0.2, 0.6),
	\sweepmin, Pwhite(440, 660),
	\sweepmax, Pwhite(6400, 8000),
	\sweepphase, Pwhite(0, pi),
	\pan, Pwhite(-0.9, 0.9),
	\dry, 0.333
) <> ~base;

~plucks = Pbind(
	\octave, Prand((4..6), inf),
	\degree, Prand((-2..9), inf),
	\dur, Pwrand([0.125, 0.25, 0.3333333, 0.5, 0.666666, 0.75, 1], [0.2, 0.4, 0.12, 0.5, 0.12, 0.12, 0.12].normalizeSum, inf),
	\suslev, Pwhite(0.05, 0.4),
	\atk, Pwhite(0.001, 0.1),
	\dec, Pwhite(0.05, 0.4),
	\rel, Pwhite(0.4, 1.8),
	\dry, Pwhite(0.5, 0.7),
	\amp, Pseq([-6, Pn(Pwhite(-12, -9), Pwhite(3, 7))].dbamp, inf),
	\pan, Pseq([
		Pseries(-0.5, 0.1, 10),
		Pseries(0.5, -0.1, 10)
	], inf),
	\rq, Pwhite(0.1),
	\sweepmin, 1200,
	\sweepmax, 1800,
	\legato, Pwhite(0.4, 0.7)
) <> ~base;
)

~p1 = ~drone.play(t);
~p1.stop;

~p2 = ~plucks.play(t);
~p2.stop;

// record it
(
~player = Pfindur(
	240, 
	Ptpar([
		0.0, ~drone,
		4.0, ~plucks
	])
);
)


// set up recording - put this in my file template
~recFNString = PathName(thisProcess.nowExecutingPath).parentPath ++ "recordings\\"++ Date.getDate.format("%Y-%m-%d_%H-%M") ++".wav";
s.record(~recFNString);

~p3 = ~player.play(t);
~p3.stop;

s.stopRecording;
// kthxbye
s.quit;
